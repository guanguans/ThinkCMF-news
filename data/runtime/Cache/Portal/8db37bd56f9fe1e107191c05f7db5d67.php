<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title><?php echo ($site_name); ?></title>
    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />
    <meta name="description" content="<?php echo ($site_seo_description); ?>">
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- Set render engine for 360 browser -->
<meta name="renderer" content="webkit">

<!-- No Baidu Siteapp-->
<meta http-equiv="Cache-Control" content="no-siteapp"/>

<!-- HTML5 shim for IE8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="/thinkcmf-demo/themes/html/Public/js/jquery.min.js"></script>
<script src="/thinkcmf-demo/themes/html/Public/js/jquery.jslides.js"></script>
<script src="/thinkcmf-demo/themes/html/Public/js/jquery.carousel.js"></script>
<link href="/thinkcmf-demo/themes/html/Public/css/css.css" rel="stylesheet">
</head>
<body>
<div class="header">
    <div class="top clearfix">
        <div class="logo">
            <h1><a href="/thinkcmf-demo">泛海扬帆</a></h1>
        </div>
        <div class="top-info">
			<h2><a href="#">基金会</a></h2>
        </div>
    </div>
    <div class="nav-box">
        <!-- <ul class="p-ul">
            <li>
                <a href="index.html">首页</a>
            </li>
            <li>
				 <a href="list.html">新闻</a>
                <ul>
                    <li><a href="list.html">最新资讯</a></li>
                    <li><a href="list.html">媒体报告</a></li>
                    <li><a href="list.html">通知公告</a></li>
                </ul>
            </li>
            <li>
                <a href="帆友港湾.html">帆友港湾</a>
            </li>
            <li>
                <a href="项目库.html">项目库</a>
            </li>
            <li>
               <a href="下载中心.html">下载中心</a>
            </li>
            <li>
				<a href="联系我们.html">关于我们</a>
                <ul>
                    <li><a href="联系我们.html">项目介绍</a></li>
                    <li><a href="联系我们.html">项目大记事</a></li>
                    <li><a href="联系我们.html">联系我们</a></li>
                </ul>
            </li>
        </ul> -->
        <?php $menu_root_ul_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a class='dropdown-toggle' href='\$href' target='\$target'>\$label</a>"; $ul_class="dropdown-menu" ; $li_class="" ; $menu_root_ul_class="nav"; $showlevel=6; $dropdown='dropdown'; ?>
        <?php echo sp_get_menu("main",$menu_root_ul_id,$filetpl,$foldertpl,$ul_class,$li_class,$menu_root_ul_class,$showlevel,$dropdown);?>
    </div>
</div>
<div id="full-screen-slider" >
    <?php $slides=sp_getslide("index_slide"); ?>
    <ul id="slides">
        <?php if(is_array($slides)): foreach($slides as $key=>$vo): ?><li style="background:url('<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>') no-repeat center;background-size: cover; "><a href="<?php echo ($vo["slide_url"]); ?>" target="_blank"></a></li><?php endforeach; endif; ?>
    </ul>
</div>
<div class="main" style="margin-bottom:0px;">
    <div class="category clearfix">
        <div class="cate-left">
            <h3 class="cate-title">
                <a href="<?php echo U('list/index',array('id'=>6));?>">more<span>>></span></a>
                <p>公司新闻</p>
            </h3>
            <?php $tag='field:posts.id,post_date,post_title,smeta;limit:9;order:post_date DESC;'; $news = sp_sql_posts_bycatid(6,$tag); ?>
            <div class="news-list">
                <ul>
                    <?php if(is_array($news)): foreach($news as $key=>$vo): $smeta=json_decode($vo['smeta'],true); $date = date("Y-m-d",strtotime($vo['post_date'])); ?>
                        <li>
                            <a href="<?php echo U('article/index',array('cid'=>6,'id'=>$vo['id']));?>"><img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" width="148" height="262"/></a>
                            <span><?php echo ($date); ?></span>
                            <a href="<?php echo U('article/index',array('cid'=>6,'id'=>$vo['id']));?>"><?php echo ($vo["post_title"]); ?></a>
                            <i></i>
                        </li><?php endforeach; endif; ?>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <div class="cate-right">
            <h3 class="cate-title">
                <a href="<?php echo U('list/index',array('id'=>8));?>">more<span>>></span></a>
                <p>通知公告</p>
            </h3>
            <?php $tag='field:posts.id,post_title,post_excerpt,smeta;limit:1;order:post_date DESC;'; $notice = sp_sql_posts_bycatid(8,$tag); $smeta=json_decode($notice[0]['smeta'],true); ?>
            <div class="notice">
                <div class="notice-desc">
                    <p><?php echo ($notice[0]['post_excerpt']); ?>...<a href="<?php echo U('article/index',array('cid'=>8,'id'=>$notice[0]['id']));?>">【查看详情】</a></p>
                </div>
                <p class="notice-img">
                    <a href="<?php echo U('article/index',array('cid'=>8,'id'=>$notice[0]['id']));?>"><img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt="" width="430" height="267"></a>
                </p>
            </div>
        </div>
    </div>
    <div class="category clearfix">
        <div class="cate-left">
            <h3 class="cate-title">
                <a href="<?php echo U('list/index',array('id'=>2));?>">more</a>
                
                <p class="c"><span>千帆扬帆</span></p>
            </h3>
            <?php $index_small=sp_getslide("index_small"); ?>
            <div class="caroursel-box">
                <div class = "caroursel poster-main" data-setting = '{
                    "width":720,
                    "height":227,
                    "posterWidth":380,
                    "posterHeight":238,
                    "scale":0.7,
                    "dealy":"2000",
                    "algin":"middle"
                    }'>
                    <ul class = "poster-list">
                        <?php if(is_array($index_small)): foreach($index_small as $key=>$vo): ?><li class = "poster-item"><img src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" width = "100%" height="100%"></li><?php endforeach; endif; ?>
                    </ul>
                    <div class = "poster-btn poster-prev-btn"></div>
                    <div class = "poster-btn poster-next-btn"></div>
                </div>
                <i></i>
                <div class="caroursel-desc">
                    <ul>
                        <?php if(is_array($index_small)): foreach($index_small as $key=>$vo): ?><li>
                                <p class="desc-first-p"><?php echo ($vo["slide_name"]); ?>：</p>
                                <p><?php echo ($vo["slide_des"]); ?>...<a href="<?php echo ($vo["slide_url"]); ?>" target="_blank">【查看详情】</a></p>
                            </li><?php endforeach; endif; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="cate-right">
            <h3 class="cate-title" style="margin-bottom:10px;">
                <a href="<?php echo U('list/index',array('id'=>2));?>">more<span>>></span></a>
                <p>帆友港湾</p>
            </h3>
            <?php $tag='cid:2;'; $posts=sp_sql_posts($tag); $count = ceil((count($posts) / 6)); for($i= 0 ; $i < $count; $i++){ $a = $i * 6; $b = ($i+1) * 6; $tag='cid:2;field:object_id,post_title,smeta;limit:'.$a.','.$b.';order:post_date asc'; $lists[$i] = $posts=sp_sql_posts($tag); } ?>
            <div class="cate-gw" >
                <div class="but">
                    <a onclick="show();" ondblclick="show();">换一批</a>
                </div>
                <?php $__FOR_START_24892__=0;$__FOR_END_24892__=$count;for($i=$__FOR_START_24892__;$i < $__FOR_END_24892__;$i+=1){ ?><ul>
                        <?php if(is_array($lists[$i])): foreach($lists[$i] as $key=>$vo): $smeta=json_decode($vo['smeta'], true); ?>
                            <li>
                                <a href="<?php echo U('article/index',array('cid'=>2,'id'=>$vo['object_id']));?>">
                                    <img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt="">
                                    <p><?php echo ($vo["post_title"]); ?></p>
                                </a>
                            </li><?php endforeach; endif; ?>
                    </ul><?php } ?>
            </div>
        </div>
    </div>
    <div class="category clearfix" style="margin-bottom:0px;">
        <div class="cate-left" style='width:100%; padding-bottom:40px;'>
            <h3 class="cate-title">
                <a href="<?php echo U('list/index',array('id'=>7));?>">more<span>>></span></a>
                <p>媒体报道</p>
            </h3>
            <div class="news-m-list clearfix">
                <?php $tag='field:posts.id,post_date,post_title,post_excerpt,smeta;limit:7;order:post_date DESC;'; $medio = sp_sql_posts_bycatid(7,$tag); ?>
                <div class="news-m-img">
                    <a href="#"><img src="/thinkcmf-demo/themes/html/Public/images/news-img-3.jpg" alt="" width="685" height="302"></a>
                </div>
                <div class="news-m-right">
                    <ul>
                        <?php if(is_array($medio)): foreach($medio as $key=>$vo): $date = date("Y-m-d",strtotime($vo['post_date'])); ?>
                            <li>
                                <span style="padding-right:30px;"><?php echo ($date); ?></span ><a href="<?php echo U('article/index',array('cid'=>7,'id'=>$vo['id']));?>"><?php echo ($vo["post_title"]); ?>....</a>
                                <p><a href="<?php echo U('article/index',array('cid'=>7,'id'=>$vo['id']));?>"><?php echo ($vo["post_excerpt"]); ?></a></p>
                            </li><?php endforeach; endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="cate-left">
            <h3 class="cate-title" style="border-bottom:1px solid #d6d6d6;height:33px;">
                <p id="gw-icon">官网入口</p>
                <p id="hz-icon">合作伙伴</p>
            </h3>
            <?php $slides=sp_getslide("gw_slide"); $links = sp_getlinks(); ?> 
            <ul class="friend" id="gw">
                <?php if(is_array($slides)): foreach($slides as $key=>$vo): ?><a href="<?php echo ($vo["slide_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["slide_name"]); ?></a><?php endforeach; endif; ?>
            </ul>
            <ul class="friend" id='yq'style="display:none;" >
                <?php if(is_array($links)): foreach($links as $key=>$vo): ?><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a><?php endforeach; endif; ?>
            </ul>
        </div>
		 <div class="cate-right" >
            <?php $slides=sp_getslide("jjh_slide"); ?>
            <h3 class="cate-title">
                <p><?php echo ($slides[0]['cat_name']); ?></p>
            </h3>
            <div class="jjh">
                <a href="<?php echo ($slides[0]['slide_url']); ?>" target="_blank">点击进入</a>
            </div>
        </div>
    </div>
</div>
<div class="ewm">
    <img src="<?php echo sp_get_asset_upload_path($slides[0]['slide_pic']);?>" alt="">
    <p><?php echo ($slides[0]['slide_des']); ?></p>
</div>
<div class="footer">
    <p>京ICP备07040087 2004-2012 中国西部人才开发基金会</p>
    <p>地址：北京市海淀区长春桥路6号国家行政学院欣正大厦6层 邮政编码：100089 邮箱：xirenhui@163.com</p>
    <p>电话：010-68928975 传真：010-68928974</p>
</div>
<script>
    $(function(){
        $(".nav-box .nav>li").hover(function(){
            $(this).addClass('on').find('ul').stop().slideDown(300);
        },function(){
            $(this).removeClass('on').find('ul').stop().slideUp(200);
        });
        $(".news-list ul li").hover(function(){
            $(".news-list ul li").removeClass('on');
            var t = $(this).index()*30;
            $(this).addClass('on');
            $(this).find('img').css('top',-t);
        },function(){
            //$(this).removeClass('on');
        });
        $(".news-m-right ul li").hover(function(){
            $(".news-m-right ul li").removeClass('on');
            $(this).addClass('on');
        });
    });
    Caroursel.init($('.caroursel'));
</script>
</body>
<script>
    // 新闻列表
    $(".news-list ul li:first").addClass("on");
    
    //媒体报告
    $(".news-m-right li:first").addClass("on");

    // 帆友港湾 默认第一个显示
    $(".cate-gw ul li:even").addClass('m-r-6');
    $(".cate-gw ul:first").css('display','block');
    // 随机显示
    function show(){
        var lists = $(".cate-gw ul");
        var len = lists.length;
        var ran = Math.floor(Math.random()*len);
        for (var i = 0; i < len; i++) {
            if(i == ran){
                $(lists[i]).css('display','block');
            }else{
                $(lists[i]).css('display','none');
            }
        };
    }

    // 官网 友情链接
    $("#gw-icon").mouseover(function(){
        $("#gw-icon").css({color:'#fff',background:'#1692ff'});
        $("#hz-icon").css({color:'black',background:'#fff'});
        $("#gw").css('display','block');
        $("#yq").css('display','none');
    });
    $("#hz-icon").mouseover(function(){
        $("#hz-icon").css({color:'#fff',background:'#1692ff'});
        $("#gw-icon").css({color:'black',background:'#fff'});
        $("#yq").css('display','block');
        $("#gw").css('display','none');
    });

    // 官网轮播
    $(function(){
        setInterval(function(){
            // 友情链接轮播：获取最后一张图片，添加到前面
            $('#gw a:last').prependTo('#gw');
            // 官网轮播
            $('#yq a:last').prependTo('#yq');
        },1500);
    })
</script>
</html>