<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace Portal\Controller;
use Common\Controller\HomebaseController;

class ListController extends HomebaseController {

	// 前台文章列表
	public function index() {
	    $term_id=I('get.id',0,'intval');
		$term=sp_get_term($term_id);
		if(empty($term)){
		    header('HTTP/1.1 404 Not Found');
		    header('Status:404 Not Found');
		    if(sp_template_file_exists(MODULE_NAME."/404")){
		        $this->display(":404");
		    }
		    return;
		}
		
		$tplname=$term["list_tpl"];
    	$tplname=sp_get_apphome_tpl($tplname, "list");
    	$this->assign($term);
    	$this->assign('cat_id', $term_id);
    	// $this->display(":$tplname");
    	if($term_id == 6){
    		$this->display(":zxzx");
    	}else if($term_id == 7){
    		$this->display(":mtbg");
    	}else if($term_id == 8){
    		$this->display(":article");
    	}else if($term_id == 2){
    		$this->display(":fygw");
    	}else if($term_id == 3){
    		$this->display(":xmk");
    	}else if($term_id == 4){
    		$this->display(":download");
    	}else if($term_id == 9){
    		$this->display(":article");
    	}else if($term_id == 10){
    		$this->display(":djs");
    	}else{
    		$this->display(":index");
    	}
	}
	
	// 文章分类列表接口,返回文章分类列表,用于后台导航编辑添加
	public function nav_index(){
		$navcatname="文章分类";
        $term_obj= M("Terms");

        $where=array();
        $where['status'] = array('eq',1);
        $terms=$term_obj->field('term_id,name,parent')->where($where)->order('term_id')->select();
		$datas=$terms;
		$navrule = array(
		    "id"=>'term_id',
            "action" => "Portal/List/index",
            "param" => array(
                "id" => "term_id"
            ),
            "label" => "name",
		    "parentid"=>'parent'
        );
		return sp_get_nav4admin($navcatname,$datas,$navrule) ;
	}

    // 附件下载
    public function file_download(){
        $article_id = I('get.id',0,'intval');
        $term_id = I('get.term_id',0,'intval');
        $posts_model = M("Posts");

        $article=$posts_model
            ->alias("a")
            ->field('a.pdf,a.post_title,c.user_login,c.user_nicename,b.term_id')
            ->join("__TERM_RELATIONSHIPS__ b ON a.id = b.object_id")
            ->join("__USERS__ c ON a.post_author = c.id")
            ->where(array('a.id'=>$article_id,'b.term_id'=>$term_id))
            ->find();
        if(!$article['pdf']){
            header('HTTP/1.1 404 Not Found');
            header('Status:404 Not Found');
            if(sp_template_file_exists(MODULE_NAME."/404")){
                $this->display(":404");
            }

            return;
        }
        $filename = SITE_PATH.'data/upload/'.$article['pdf'];
        $ph_info = pathinfo($filename);
        $showname = $article['post_title'] .'.'.$ph_info['extension'];
        require THINK_PATH.'Library/Org/Net/Http.class.php';
        \Org\Net\Http::download($filename,$showname);
    }
}
